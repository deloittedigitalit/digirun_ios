//
//  LocationManager.swift
//  RunDigital
//
//  Created by Musolino, Antonino Francesco (IT - Milano) on 19/03/2018.
//  Copyright © 2018 Musolino, Antonino Francesco. All rights reserved.
//

import CoreLocation

class LocationManager
{
	static let shared = CLLocationManager()

	private init() { }
}
