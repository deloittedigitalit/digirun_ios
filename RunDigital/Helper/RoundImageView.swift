//
//  RoundImageView.swift
//  Estra
//
//  Created by Musolino, Antonino Francesco (IT - Milano) on 14/04/2017.
//  Copyright © 2017 Musolino, Antonino Francesco (IT - Milano). All rights reserved.
//

import Foundation
import UIKit

class RoundImageView: UIImageView
{
	
	@IBInspectable var bordered:Bool = false
	
	@IBInspectable var borderColor: UIColor = .clear
	
	@IBInspectable var borderWidth: CGFloat = 1
	
	override func awakeFromNib()
	{
		self.layer.cornerRadius = self.bounds.width / 2
		self.clipsToBounds = true
		
		if bordered
		{
			self.layer.borderWidth = borderWidth
			self.layer.borderColor = borderColor.cgColor
		}
	}
}
