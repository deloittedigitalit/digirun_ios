//
//  ViewControllerExtensions.swift
//  RunDigital
//
//  Created by Musolino, Antonino Francesco (IT - Milano) on 19/03/2018.
//  Copyright © 2018 Musolino, Antonino Francesco. All rights reserved.
//

import UIKit

extension UIResponder
{
	func getParentViewController() -> UIViewController?
	{
		if self.next is UIViewController
		{
			return self.next as? UIViewController
		} else {
			if self.next != nil {
				return (self.next!).getParentViewController()
			} else {return nil}
		}
	}
}

extension UINavigationController
{
	open override var childViewControllerForStatusBarStyle: UIViewController?
	{
		return visibleViewController
	}
}

extension UITabBarController
{
	open override var childViewControllerForStatusBarStyle: UIViewController?
	{
		return selectedViewController
	}
}
