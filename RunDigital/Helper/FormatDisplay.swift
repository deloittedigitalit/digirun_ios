//
//  FormatDisplay.swift
//  RunDigital
//
//  Created by Musolino, Antonino Francesco (IT - Milano) on 19/03/2018.
//  Copyright © 2018 Musolino, Antonino Francesco. All rights reserved.
//

import Foundation
struct FormatDisplay
{
	static func distance(_ distance: Double) -> String {
		let distanceMeasurement = Measurement(value: distance, unit: UnitLength.meters)
		return FormatDisplay.distance(distanceMeasurement)
	}

	static func distance(_ distance: Measurement<UnitLength>) -> String {
		let formatter = MeasurementFormatter()
		formatter.unitStyle = .short
		formatter.numberFormatter.maximumFractionDigits = 1
		return formatter.string(from: distance)
	}

	static func time(_ seconds: Int) -> String {
		let formatter = DateComponentsFormatter()
		formatter.allowedUnits = [.hour, .minute, .second]
		formatter.unitsStyle = .positional
		formatter.zeroFormattingBehavior = .pad
		return formatter.string(from: TimeInterval(seconds))!
	}

	static func pace(distance: Measurement<UnitLength>, seconds: Int, outputUnit: UnitSpeed) -> String {
		let formatter = MeasurementFormatter()
		formatter.unitOptions = [.providedUnit]
		formatter.numberFormatter.currencyDecimalSeparator = ":"
		formatter.numberFormatter.decimalSeparator = ":"
		formatter.numberFormatter.maximumFractionDigits = 2
		formatter.numberFormatter.maximumIntegerDigits = 2
		formatter.numberFormatter.minimumFractionDigits = 2
		formatter.numberFormatter.minimumIntegerDigits = 2
		let speedMagnitude = seconds != 0 ? distance.value / Double(seconds) : 0
		let speed = Measurement(value: speedMagnitude, unit: UnitSpeed.metersPerSecond)
		return formatter.string(from: speed.converted(to: outputUnit))
	}

	static func date(_ timestamp: Date?) -> String {
		guard let timestamp = timestamp as Date? else { return "" }
		let formatter = DateFormatter()
		formatter.dateStyle = .medium
		return formatter.string(from: timestamp)
	}
}
