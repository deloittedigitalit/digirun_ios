//
//  User.swift
//  RunDigital
//
//  Created by Musolino, Antonino Francesco (IT - Milano) on 19/03/2018.
//  Copyright © 2018 Musolino, Antonino Francesco. All rights reserved.
//

import Foundation
import Gloss

struct User: JSONDecodable
{
	var name: String?
	var surname: String?

	init?(json: JSON)
	{
		self.name = "user.first_name" <~~ json
		self.surname = "user.last_name" <~~ json
	}

}
