//
//  SharedConnectionData.swift
//  RunDigital
//
//  Created by Musolino, Antonino Francesco (IT - Milano) on 13/03/2018.
//  Copyright © 2018 Musolino, Antonino Francesco. All rights reserved.
//

import Foundation

/// La classe **Singleton** che contiene tutti i dati salvati ed il token di connessione.
class SharedData
{
	static let sharedInstance = SharedData()

	private init()
	{
		savedToken = Token()
	}

	var savedToken: Token

	/// Il metodo permette di salvare all'interno delle UserDefaults il token di connessione
	func SaveTokenToUserDefaults()
	{
		let savedData = NSKeyedArchiver.archivedData(withRootObject: savedToken)
		UserDefaults.standard.set(savedData, forKey: "SavedToken")
	}

	func removeToken()
	{
		savedToken.accessToken = nil
		UserDefaults.standard.removeObject(forKey: "SavedToken")
	}

}

public class Token : NSObject,NSCoding
{
	var accessToken: String?
	var expiryTime: Date?

	override init()
	{
		if let savedToken = UserDefaults.standard.object(forKey: "SavedToken") as? Data
		{
			let newToken = NSKeyedUnarchiver.unarchiveObject(with: savedToken) as! Token
			accessToken = newToken.accessToken
			expiryTime = newToken.expiryTime
		}
		else
		{
			accessToken = ""
			expiryTime = Date()
		}
	}

	init(token:String, expiry:Date?)
	{
		self.accessToken = token
		if expiry != nil
		{
			self.expiryTime = expiry!
		}
		else
		{
			self.expiryTime = Calendar.current.date(byAdding: .day, value: 1, to: Date())!
		}

	}

	required convenience public init?(coder decoder: NSCoder)
	{
		guard let token = decoder.decodeObject(forKey: "AccessToken") as? String,
			let expiry = decoder.decodeObject(forKey: "ExpiryTime") as? Date
			else { return nil }

		self.init(token: token, expiry:expiry)
	}

	public func encode(with aCoder: NSCoder)
	{
		aCoder.encode(self.accessToken, forKey: "AccessToken")
		aCoder.encode(self.expiryTime, forKey: "ExpiryTime")
	}

	func isNull() -> Bool
	{
		return accessToken == nil || accessToken == ""
	}

	func isExpired() -> Bool
	{
		guard self.expiryTime != nil
			else
		{
			return true
		}

		if Date().compare(self.expiryTime!) == .orderedDescending
		{
			return true
		}
		else
		{
			return false
		}
	}

	func clearToken()
	{
		accessToken = nil
		expiryTime = nil
	}
}
