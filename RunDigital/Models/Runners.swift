//
//  Runners.swift
//  RunDigital
//
//  Created by Musolino, Antonino Francesco (IT - Milano) on 06/03/2018.
//  Copyright © 2018 Musolino, Antonino Francesco. All rights reserved.
//

import Foundation
import MapKit
import Gloss

class Runner: NSObject, MKAnnotation, JSONDecodable
{
	let coordinate: CLLocationCoordinate2D
	var name: String?
	var surname: String?
	var indirizzo: String?
	var remainDistance: Double?
	var walkedDistance: Double?
	var isCurrentUser: Bool?
	var gapToNext: Double?
	var gapToFirst: Double?
	var title: String?
	var hasFinished: Bool = false
	var time: Double?

	init(latitudine: Double, longitudine: Double, name: String?)
	{
		self.coordinate = CLLocationCoordinate2D(latitude:latitudine, longitude:longitudine)
		self.name = name
		super.init()
	}

	required init?(json: JSON)
	{
		let latitude: Double = "latitude" <~~ json ?? 0
		let longitude: Double = "longitude" <~~ json ?? 0
		self.coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
		self.name = "first_name" <~~ json
		self.surname = "last_name" <~~ json
		self.walkedDistance = "walked_distance" <~~ json
		self.remainDistance = "remaining_distance" <~~ json
		self.isCurrentUser = "current_player" <~~ json
		self.gapToNext = "gap_to_next" <~~ json
		self.gapToFirst = "gap_to_first" <~~ json
		self.title = "\(self.name ?? "") \(self.surname ?? "")"
		self.time = "total_time" <~~ json
		super.init()
	}
}
