//
//  AppDelegate.swift
//  RunDigital
//
//  Created by Musolino, Antonino Francesco (IT - Milano) on 06/03/2018.
//  Copyright © 2018 Musolino, Antonino Francesco. All rights reserved.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

	var window: UIWindow?


	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool
	{
		UIApplication.shared.isIdleTimerDisabled = true
		IQKeyboardManager.sharedManager().enable = true
		IQKeyboardManager.sharedManager().shouldShowToolbarPlaceholder = false
		IQKeyboardManager.sharedManager().toolbarDoneBarButtonItemText = "Conferma"
		UINavigationBar.appearance().barTintColor = UIColor(red: 64/255, green: 64/255, blue: 64/255, alpha: 1)
		UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: "OpenSans-Light", size: 17.0)!, NSAttributedStringKey.foregroundColor : UIColor.white]
		UINavigationBar.appearance().tintColor = .white
		UINavigationBar.appearance().isTranslucent = false

		if let token = SharedData.sharedInstance.savedToken.accessToken, !token.isEmpty
		{
			startFromMainPage()
		}
		return true
	}

	func startFromMainPage()
	{
		let storyBoard = UIStoryboard(name: "Main", bundle: nil)
		let runningView = storyBoard.instantiateViewController(withIdentifier: "RunningViewViewController")
		let navigationController = UINavigationController(rootViewController: runningView)
		self.window?.rootViewController = navigationController
		self.window?.makeKeyAndVisible()
	}

	func applicationWillResignActive(_ application: UIApplication)
	{
		// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
		// Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
	}

	func applicationDidEnterBackground(_ application: UIApplication) {
		// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
		// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
		CoreDataStack.saveContext()
	}

	func applicationWillEnterForeground(_ application: UIApplication) {
		// Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
	}

	func applicationDidBecomeActive(_ application: UIApplication) {
		// Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
	}

	func applicationWillTerminate(_ application: UIApplication) {
		// Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
		// Saves changes in the application's managed object context before the application terminates.
		CoreDataStack.saveContext()
	}

}

