//
//  NetworkResponse.swift
//  RunDigital
//
//  Created by Musolino, Antonino Francesco (IT - Milano) on 13/03/2018.
//  Copyright © 2018 Musolino, Antonino Francesco. All rights reserved.
//

import Foundation

struct NetResponse
{
	var code:Int
	var message:String
	var data: Any?

	init(codice: Int, messaggio:String, data:Any? = nil)
	{
		self.code = codice
		self.message = messaggio
		self.data = data
	}
}

struct NetCode
{
	static let success = 200
	static let notFound = 404
	static let badrequest = 400
	static let unauthorized = 401
	static let requestFail = 000
}
