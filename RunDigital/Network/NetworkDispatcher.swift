//
//  NetworkHandler.swift
//  RunDigital
//
//  Created by Musolino, Antonino Francesco (IT - Milano) on 13/03/2018.
//  Copyright © 2018 Musolino, Antonino Francesco. All rights reserved.
//

import Foundation
import Alamofire
import Gloss

public struct Environment
{
	// Base URL of the environment
	let host: String = "https://digirun.ddprototypes.com/api/v2/"

	/// This is the list of common headers which will be part of each Request
	var headers: [String: String] = ["Content-Type": "application/json"]
}

protocol Dispatcher
{
	var environment: Environment {get}
}


open class NetworkDispatcher: Dispatcher
{

	var currentRequest: Alamofire.Request?

	public var environment: Environment

	let AlamofireManager:SessionManager

	init()
	{
		environment = Environment()
		AlamofireManager = Alamofire.SessionManager.default
		AlamofireManager.session.configuration.timeoutIntervalForRequest = 60
	}

	func saveTokenAsync(_ newToken:String)
	{
		SharedData.sharedInstance.savedToken = Token(token: newToken, expiry: nil)
		SharedData.sharedInstance.SaveTokenToUserDefaults()
	}

	func enrollUserRequest(_ request: Request, completionHandler:((Any?,Error?)->())?)
	{
		let full_url = "\(environment.host)\(request.path)"
		request.headers?.forEach { (k,v) in environment.headers[k] = v }

		if currentRequest != nil
		{
			currentRequest?.cancel()
		}

		currentRequest = AlamofireManager.request(full_url, method:request.method , parameters: request.parameters, encoding: request.encoding, headers: environment.headers).responseJSON
			{	responseData in

				if(responseData.result.isSuccess)
				{

					switch responseData.response!.statusCode
					{
					case 200,201:
						let newToken = "\(request.parameters!["email"]!):\(request.parameters!["verification_code"]!)".md5()
						self.saveTokenAsync(newToken)
						var user: User?
						if let resultJSON = responseData.result.value as? JSON
						{
							user = User.init(json: resultJSON)
						}
						completionHandler?(NetResponse(codice: NetCode.success, messaggio: "Login Effettuato", data: user),nil)
					default:
						completionHandler?(NetResponse(codice: responseData.response!.statusCode, messaggio: "Errore generico"),nil)
					}

				}
				else
				{
					completionHandler?(NetResponse(codice: NetCode.requestFail, messaggio: "Errore di rete"),responseData.result.error!)
				}
		}
	}

	func getAllRunnersRequest(_ request: Request, completionHandler:((Any?,Error?)->())?)
	{
		let full_url = "\(environment.host)\(request.path)"
		request.headers?.forEach { (k,v) in environment.headers[k] = v }

		if currentRequest != nil
		{
			currentRequest?.cancel()
		}

		currentRequest = AlamofireManager.request(full_url, method:request.method , parameters: request.parameters, encoding: request.encoding, headers: environment.headers).responseJSON
			{	responseData in

				if(responseData.result.isSuccess)
				{

					switch responseData.response!.statusCode
					{
					case 200,201:
						guard let parsedJSON = responseData.result.value as? [String:Any]
							else
						{
							completionHandler?(NetResponse(codice: responseData.response!.statusCode, messaggio: "Errore generico"),nil)
							return
						}

						var runners: [Runner] = []
						if let arrivedRunners = parsedJSON.valueForKeyPath(keyPath: "arrived") as? [[String:Any]]
						{
							runners = [Runner].from(jsonArray: arrivedRunners)!
							for runner in runners
							{
								runner.hasFinished = true
							}
						}
						if let runningRunners = parsedJSON.valueForKeyPath(keyPath: "running") as? [[String:Any]]
						{
							runners.append(contentsOf: [Runner].from(jsonArray: runningRunners)!)
						}
						completionHandler?(NetResponse(codice: NetCode.success, messaggio: "Lista scaricata", data: runners),nil)
					default:
						completionHandler?(NetResponse(codice: responseData.response!.statusCode, messaggio: "Errore generico"),nil)
					}

				}
				else
				{
					completionHandler?(NetResponse(codice: NetCode.requestFail, messaggio: "Errore di rete"),responseData.result.error!)
				}
		}
	}

	func updateRunnerStatusRequest(_ request: Request, completionHandler:((Any?,Error?)->())?)
	{
		let full_url = "\(environment.host)\(request.path)"
		request.headers?.forEach { (k,v) in environment.headers[k] = v }

		if currentRequest != nil
		{
			currentRequest?.cancel()
		}

		currentRequest = AlamofireManager.request(full_url, method:request.method , parameters: request.parameters, encoding: request.encoding, headers: environment.headers).responseJSON
			{	responseData in

				if(responseData.result.isSuccess)
				{

					switch responseData.response!.statusCode
					{
					case 200,201:
						var remainingDistance: Double?
						if let resultJSON = responseData.result.value as? JSON
						{
							remainingDistance = resultJSON["remaining_distance"] as? Double
						}
						completionHandler?(NetResponse(codice: NetCode.success, messaggio: "Update Status effettuato", data: remainingDistance),nil)
					default:
						completionHandler?(NetResponse(codice: responseData.response!.statusCode, messaggio: "Errore generico"),nil)
					}

				}
				else
				{
					completionHandler?(NetResponse(codice: NetCode.requestFail, messaggio: "Errore di rete"),responseData.result.error!)
				}
		}
	}

	func startRunRequest(_ request: Request, completionHandler:((Any?,Error?)->())?)
	{
		let full_url = "\(environment.host)\(request.path)"
		request.headers?.forEach { (k,v) in environment.headers[k] = v }

		if currentRequest != nil
		{
			currentRequest?.cancel()
		}

		currentRequest = AlamofireManager.request(full_url, method:request.method , parameters: request.parameters, encoding: request.encoding, headers: environment.headers).responseJSON
			{	responseData in

				if(responseData.result.isSuccess)
				{

					switch responseData.response!.statusCode
					{
					case 200,201:
						completionHandler?(NetResponse(codice: NetCode.success, messaggio: "Update Status effettuato"),nil)
					default:
						completionHandler?(NetResponse(codice: responseData.response!.statusCode, messaggio: "Errore generico"),nil)
					}
				}
				else
				{
					completionHandler?(NetResponse(codice: NetCode.requestFail, messaggio: "Errore di rete"),responseData.result.error!)
				}
		}
	}
}
