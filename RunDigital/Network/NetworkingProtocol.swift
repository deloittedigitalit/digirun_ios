//
//  NetworkingProtocol.swift
//  RunDigital
//
//  Created by Musolino, Antonino Francesco (IT - Milano) on 13/03/2018.
//  Copyright © 2018 Musolino, Antonino Francesco. All rights reserved.
//

import Foundation
import Alamofire
import MapKit

public protocol Request
{
	/// Relative path of the endpoint
	var path: String { get }

	/// This define the HTTP method we should use to perform the call
	/// We have defined it inside an String based enum called `HTTPMethod`
	/// just for clarity
	var method: HTTPMethod { get }

	/// These are the parameters we need to send along with the call.
	/// Params can be passed into the body or along with the URL
	var parameters: [String: Any]? { get }

	/// You may also define a list of headers to pass along with each request.
	var headers: [String: String]? { get }

	var encoding: ParameterEncoding {get}
}

public enum RunnerUpdate: Request
{
	case updateStatus(location: CLLocationCoordinate2D, pace: Double)
	case startRun()

	public var path: String
	{
		switch self
		{
		case .updateStatus(location: _, pace: _):
			return "trackings/new"
		case .startRun():
			return "trackings/start_run"
		}
	}

	public var method: HTTPMethod
	{
		switch self
		{
		case .updateStatus(_,_):
			return .post
		case .startRun():
			return .post
		}
	}

	public var parameters: [String: Any]?
	{
		switch self
		{
		case .updateStatus(let location, let pace):
			return ["latitude": location.latitude, "longitude": location.longitude, "pace": pace]
		case .startRun():
			return nil
		}
	}

	public var headers: [String : String]?
	{
		return ["Authorization" : SharedData.sharedInstance.savedToken.accessToken!]
	}


	public var encoding : ParameterEncoding
	{
		switch self.method
		{
		case .get:
			return URLEncoding.default
		default:
			return JSONEncoding.default
		}
	}
}

public enum RunnerRequests: Request
{
	case getAll()

	public var path: String
	{
		return "players/all"
	}

	public var method: HTTPMethod
	{
		switch self
		{
		case .getAll():
			return .get
		}
	}

	public var parameters: [String: Any]?
	{
		switch self
		{
		case .getAll():
			return nil
		}
	}

	public var headers: [String : String]?
	{
		return [ "Authorization" : SharedData.sharedInstance.savedToken.accessToken!]
	}


	public var encoding : ParameterEncoding
	{
		switch self.method
		{
		case .get:
			return URLEncoding.default
		default:
			return JSONEncoding.default
		}
	}
}

public enum UserRequests: Request
{
	case enroll(user: WelcomePage.User)

	public var path: String
	{
		return "players/enroll"
	}

	public var method: HTTPMethod
	{
		switch self
		{
		case .enroll(_):
			return .post
		}
	}

	public var parameters: [String: Any]?
	{
		switch self
		{
		case .enroll(let user):
			return ["first_name" : user.name ?? "", "last_name" : user.surname ?? "", "email" : user.email ?? "", "verification_code" : user.verificationCode ?? ""]
		}
	}

	public var headers: [String : String]?
	{
		switch self
		{
		default:
			return nil
		}
	}


	public var encoding : ParameterEncoding
	{
		switch self.method
		{
		case .get:
			return URLEncoding.default
		default:
			return JSONEncoding.default
		}
	}
}
