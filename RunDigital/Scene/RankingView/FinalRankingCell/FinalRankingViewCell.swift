//
//  FinalRankingViewCell.swift
//  RunDigital
//
//  Created by Musolino, Antonino Francesco (IT - Milano) on 21/03/2018.
//  Copyright © 2018 Musolino, Antonino Francesco. All rights reserved.
//

import UIKit

class FinalRankingViewCell: UITableViewCell
{

	@IBOutlet weak var positionLabel: UILabel!
	@IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var myCell: UIView!
	@IBOutlet weak var timeLabel: UILabel!
	override func awakeFromNib()
	{
		super.awakeFromNib()
		myCell.layer.borderColor = UIColor(red: 141/255, green: 241/255, blue: 0, alpha: 1).cgColor
		myCell.layer.borderWidth = 1
	}

	override func prepareForReuse()
	{
		super.prepareForReuse()
		nameLabel.textColor = .white
		positionLabel.textColor = .white
		timeLabel.textColor = .white
		myCell.isHidden = true
	}

	override func setSelected(_ selected: Bool, animated: Bool)
	{
		super.setSelected(selected, animated: animated)
	}

	func prepareCell(_ runner: Runner, position: Int)
	{
		if position == 0
		{
			nameLabel.textColor = UIColor(red: 141/255, green: 241/255, blue: 0, alpha: 1)
			positionLabel.textColor = UIColor(red: 141/255, green: 241/255, blue: 0, alpha: 1)
			timeLabel.textColor = UIColor(red: 141/255, green: 241/255, blue: 0, alpha: 1)
		}
		positionLabel.text = "\(position + 1)"
		nameLabel.text = runner.name

		timeLabel.text = formatTime(runner.time ?? 0)
		if runner.isCurrentUser ?? false
		{
			myCell.isHidden = false
		}
	}

	func formatTime(_ time: Double) -> String?
	{
		let formatter = DateComponentsFormatter()
		formatter.allowedUnits = [.hour, .minute, .second, .nanosecond]
		formatter.unitsStyle = .positional
		formatter.zeroFormattingBehavior = [ .pad ]
		return formatter.string(from: TimeInterval(time))
	}

}
