//
//  RankingHeaderTableViewCell.swift
//  RunDigital
//
//  Created by Musolino, Antonino Francesco (IT - Milano) on 21/03/2018.
//  Copyright © 2018 Musolino, Antonino Francesco. All rights reserved.
//

import UIKit

class RankingHeaderTableViewCell: UITableViewCell
{

    override func awakeFromNib()
	{
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool)
	{
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
