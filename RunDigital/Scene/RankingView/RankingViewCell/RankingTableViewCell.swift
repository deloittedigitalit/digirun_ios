//
//  RankingTableViewCell.swift
//  RunDigital
//
//  Created by Musolino, Antonino Francesco (IT - Milano) on 20/03/2018.
//  Copyright © 2018 Musolino, Antonino Francesco. All rights reserved.
//

import UIKit

class RankingTableViewCell: UITableViewCell
{
	@IBOutlet weak var positionLabel: UILabel!
	@IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var toFirstLabel: UILabel!
	@IBOutlet weak var toNextLabel: UILabel!
	@IBOutlet weak var myCell: UIView!

    override func awakeFromNib()
	{
        super.awakeFromNib()
        myCell.layer.borderColor = UIColor(red: 141/255, green: 241/255, blue: 0, alpha: 1).cgColor
		myCell.layer.borderWidth = 1
    }

    override func setSelected(_ selected: Bool, animated: Bool)
	{
        super.setSelected(selected, animated: animated)
    }

	override func prepareForReuse()
	{
		super.prepareForReuse()
		positionLabel.textColor = .white
		toFirstLabel.isHidden = false
		toNextLabel.isHidden = false
		myCell.isHidden = true
	}

	func prepareCell(_ runner: Runner, position: Int)
	{
		if position == 0
		{
			toFirstLabel.isHidden = true
			toNextLabel.isHidden = true
			positionLabel.textColor = UIColor(red: 141/255, green: 241/255, blue: 0, alpha: 1)
		}
		positionLabel.text = "\(position + 1)"
		nameLabel.text = runner.name

		if let gapToFirst = runner.gapToFirst
		{
			let gap = gapToFirst != 0 ? gapToFirst / 1000 : 0
			toFirstLabel.text = String(format: "%.1fKm", gap)
		}
		if let gapToNext = runner.gapToNext
		{
			let gap = gapToNext != 0 ? gapToNext / 1000 : 0
			toNextLabel.text = String(format: "%.1fKm", gap)
		}

		if runner.isCurrentUser ?? false
		{
			myCell.isHidden = false
		}
	}
    
}
