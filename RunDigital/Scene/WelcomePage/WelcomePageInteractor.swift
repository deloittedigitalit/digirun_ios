//
//  WelcomePageInteractor.swift
//  RunDigital
//
//  Created by Musolino, Antonino Francesco (IT - Milano) on 06/03/2018.
//  Copyright (c) 2018 Musolino, Antonino Francesco. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

protocol WelcomePageBusinessLogic
{
	func registerUser(_ user: WelcomePage.User)
	func generateInvitationCode()
	var verificationCode: String {get}
}

protocol WelcomePageDataStore
{
	var verificationCode: String {get}
	var enrolledUser: User? {get}
}

class WelcomePageInteractor: WelcomePageBusinessLogic, WelcomePageDataStore
{
	var presenter: WelcomePagePresentationLogic?
	var worker: WelcomePageWorker?

	var verificationCode: String = ""

	var enrolledUser: User?
	
	func registerUser(_ user: WelcomePage.User)
	{
		worker = WelcomePageWorker()
		if let missingFields = worker?.checkMissingFields(user), missingFields.count > 0
		{
			self.presenter?.missingFieldsError(missingFields)
			return
		}
		worker?.enrollUser(user, completionHandler:
		{ response,error in
			let responseObject = response as! NetResponse
			if responseObject.code == NetCode.success
			{
				self.enrolledUser = responseObject.data as? User
				self.presenter?.presentEnrollResult(responseObject)
			} else
			{
				self.presenter?.presentErrorMessage()
			}
		})
	}

	func generateInvitationCode()
	{
		worker = WelcomePageWorker()
		verificationCode = worker!.getVerificationCode()
	}
}
