//
//  CoreDataStack.swift
//  RunDigital
//
//  Created by Musolino, Antonino Francesco (IT - Milano) on 06/03/2018.
//  Copyright © 2018 Musolino, Antonino Francesco. All rights reserved.
//

import CoreData

class CoreDataStack
{
  
  static let persistentContainer: NSPersistentContainer = {
    let container = NSPersistentContainer(name: "RunDigital")
    container.loadPersistentStores { (_, error) in
      if let error = error as NSError? {
        fatalError("Unresolved error \(error), \(error.userInfo)")
      }
    }
    return container
  }()
  
  static var context: NSManagedObjectContext { return persistentContainer.viewContext }
  
  class func saveContext () {
    let context = persistentContainer.viewContext
    
    guard context.hasChanges else {
      return
    }
    
    do {
      try context.save()
    } catch {
      let nserror = error as NSError
      fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
    }
  }
}
